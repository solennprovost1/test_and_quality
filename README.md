## Générateur de Mots de Passe Sécurisés

Ce projet est une application web permettant de générer des mots de passe sécurisés en fonction de différents
paramètres. L'application a été développée en utilisant le framework Flask pour Python.

### Fonctionnalités

- Génération de mots de passe avec des caractères alphabétiques (majuscules et minuscules).
- Possibilité d'inclure des chiffres dans le mot de passe.
- Possibilité d'inclure des symboles de ponctuation dans le mot de passe.
- Exclusion automatique du nom et du prénom de l'utilisateur dans le mot de passe généré.
- Hachage SHA512 du mot de passe généré pour une sécurité accrue.
- Utilisation d'un sel aléatoire concaténé avec le mot de passe avant le hachage pour renforcer la sécurité.
- Affichage de la puissance du mot de passe généré
- Copie automatique du mot de passe généré dans le presse papier

### Dépendances

- Flask==2.3.3 : Framework web pour Python.
- pytest==7.4.1 : Framework de test pour Python.
- selenium==4.12.0 : Librairie pour l'automatisation des tests web.
- hashlib==
- secret==
- urllib3==2.0.4 : Librairie HTTP pour Python.
- pylint==2.17.5 : Outil de vérification statique du code Python.

### Installation

1. Clonez le dépôt depuis GitHub :

   ```bash
   git clone https://github.com/votre-utilisateur/generateur-de-mots-de-passe.git

2. Installez les dépendances requises en utilisant pip :

   ```bash
   pip install -r requirements.txt

3. Exécutez l'application :

    ```bash
   python main.py

L'application sera accessible à l'adresse `http://localhost:5000/` dans votre navigateur.

### Utilisation

1. Ouvrez votre navigateur et accédez à l'adresse `http://localhost:5000/`.

2. Remplissez le formulaire avec votre nom, prénom, longueur souhaitée pour le mot de passe, et cochez les cases pour
   inclure des chiffres et/ou des symboles.

3. Cliquez sur le bouton "Générer le mot de passe" pour obtenir un mot de passe sécurisé.

4. Le mot de passe **généré** sera affiché, ainsi que sa **puissance** et son **hachage SHA512 salé**.

## Bugs rencontrés

1. Exécution rapide des scripts : Parfois, les scripts s'exécutent plus rapidement que la génération du mot de passe, ce qui entraîne un problème d'affichage du mot de passe généré. Pour résoudre ce problème, 
   une condition a été ajoutée pour vérifier que le mot de passe généré est entièrement chargé avant de passer sur l'excution des scripts.
2. Problèmes d'indentation : Pendant le développement, des problèmes d'indentation sont apparus dans le code en raison d'une mauvaise mise en forme. Grâce à l'outil d'indentation de PyCharm, on a pu résoudre rapidement ces problèmes.
3. L'utilisation de fichiers externes dans un template HTML utilisé par Flask

## Contributeurs

- Nader GOUASMI & Solenn PROVOST

## Licence
Ce projet est sous licence MIT.
