import pytest
import string
from main import PasswordGenerator
from password_utils import password_power, hash_password


def test_generate_password_with_only_letters():
    # Verifie si le mot de passe ne contient ni chiffres ni symboles
    password = PasswordGenerator.generate("John", "Doe", 12, False, False)
    assert not any(c.isdigit() for c in password)
    assert not any(c in string.punctuation for c in password)


def test_generate_password():
    # Verifie si le mot de passe contient lettres, chiffres et symboles
    password = PasswordGenerator.generate("John", "Doe", 25, True, True)
    assert any(c.isalpha() for c in password)
    assert any(c.isdigit() for c in password)
    assert any(c in string.punctuation for c in password)


def test_generate_password_no_symbols():
    # Verifie si le mot de passe ne contient aucun symboles
    password = PasswordGenerator.generate("John", "Doe", 12, True, False)
    assert not any(c in string.punctuation for c in password)


def test_generate_password_no_digits():
    # Verifie si le mot de passe ne contient aucun chiffres
    password = PasswordGenerator.generate("John", "Doe", 12, False, True)
    assert not any(c.isdigit() for c in password)


def test_generate_password_no_firstname_lastname():
    # Verifie si le mot de passe ne contient ni le nom ni le prénom
    password = PasswordGenerator.generate("John", "Doe", 12, True, True)
    assert "John" not in password
    assert "Doe" not in password


def test_generate_password_float_length():
    # Verifie si le mot de passe est de taille entière et non float comme le pramètre length
    password = PasswordGenerator.generate("John", "Doe", 12.5, True, True)
    assert len(password) == 12


def test_generate_password_zero_length():
    # Verifie si le mot de passe est de taille 8 et ne lève pas d'erreur à cause du 0
    password = PasswordGenerator.generate("John", "Doe", 0, True, True)
    assert len(password) == 8


def test_generate_password_negative_length():
    # Verifie si le mot de passe est de taille 8 et ne lève pas d'erreur à cause d'un chiffre négatif
    password = PasswordGenerator.generate("John", "Doe", -3, True, True)
    assert len(password) == 8


def test_generate_password_short_length():
    password = PasswordGenerator.generate("John", "Doe", 1, True, True)
    assert len(password) == 8


def test_password_power_length_8():
    power = password_power(8, False, False)
    assert power == "faible"


def test_password_power_length_9_and_digits():
    power = password_power(9, True, False)
    assert power == "moyen"


def test_password_power_length_13_and_digits():
    power = password_power(13, True, False)
    assert power == "fort"


def test_password_power_length_13_without_digits_or_symbols():
    power = password_power(13, False, False)
    assert power == "faible"


def test_hash_password():

    # Crée un mot de passe et calcule son hachage
    password = "mot_de_passe_secret"
    hashed_password = hash_password(password)

    # Vérifie que le hachage n'est pas vide
    assert hashed_password is not None

    # Vérifie que le hachage est une chaîne hexadécimale de 128 caractères
    assert len(hashed_password) == 128
    assert all(c in "0123456789abcdef" for c in hashed_password)

    # Vérifie que le hachage est différent pour des mots de passe différents
    different_password = "un_autre_mot_de_passe"
    different_hash = hash_password(different_password)
    assert hashed_password != different_hash


if __name__ == '__main__':
    pytest.main()
