from flask import Flask
from selenium import webdriver
from main import PasswordGeneratorApp

app = Flask(__name__)

if __name__ == "__main__":
    # Lancement de l'application Flask dans un thread séparé
    from threading import Thread


    def flask_thread():
        password_app = PasswordGeneratorApp()
        password_app.run()


    flask_server = Thread(target=flask_thread)
    flask_server.start()

    # Configuration du webdriver Firefox
    driver = webdriver.Firefox()
    driver.get("http://127.0.0.1:5000")  # URL de votre application Flask

    # Exemple de test Selenium : remplir le formulaire et soumettre
    firstname_input = driver.find_element("name", "firstname")
    lastname_input = driver.find_element("name", "lastname")
    length_input = driver.find_element("name", "length")
    use_digits_checkbox = driver.find_element("name", "use_digits")
    submit_button = driver.find_element("name", "generate_button")

    import time

    lastname_input.send_keys("Doe")
    time.sleep(2)

    firstname_input.send_keys("John")
    time.sleep(2)

    length_input.send_keys("12")
    time.sleep(2)

    use_digits_checkbox.click()
    submit_button.click()

    # Attendre quelques secondes pour que la page se recharge avec les résultats

    time.sleep(8)

    # Exemple de vérification des résultats
    password_element = driver.find_element("id", "password")
    password_hash_element = driver.find_element("id", "password_hash")

    print("Generated Password:", password_element.get_attribute("value"))
    print("Password Hash:", password_hash_element.get_attribute("value"))

    # Fermer le navigateur WebDriver
    driver.quit()

    # Arrêter le serveur Flask
    flask_server.join()
