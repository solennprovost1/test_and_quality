import hashlib
import secrets


def hash_password(password):
    salt = secrets.token_hex(16)  # Génère un sel aléatoire

    # Concatène le sel au mot de passe
    salted_password = salt + password

    # Utilise hashlib pour créer un hachage SHA-512 du mot de passe salé
    sha512 = hashlib.sha512()
    sha512.update(salted_password.encode('utf-8'))
    password_hash = sha512.hexdigest()

    return password_hash


def password_power(length, use_digits, use_symbols):
    if length == 8:
        power_password = "faible"
    elif 8 < length <= 12 and (use_digits or use_symbols):
        power_password = "moyen"
    elif length > 12 and (use_digits or use_symbols):
        power_password = "fort"
    else:
        power_password = "faible"
    return power_password
