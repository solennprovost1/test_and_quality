import secrets
import string

from flask import Flask, render_template, request
from password_utils import hash_password, password_power

app = Flask(__name__)
app.static_folder = 'templates'


class PasswordGenerator:
    @staticmethod
    def generate(firstname, lastname, length, use_digits, use_symbols):

        # Si la longueur est inférieure à 8, réinitialisez-la à 8.
        if length < 8:
            length = 8

        # Vérifie si la longueur est un nombre entier, sinon, convertir en entier.
        if not isinstance(length, int) and isinstance(length, float):
            length = int(length)

        characters = string.ascii_letters
        if use_digits:
            characters += string.digits
        if use_symbols:
            characters += string.punctuation

        password = ''.join(secrets.choice(characters) for _ in range(length))

        # Vérifiez si le nom et le prénom sont présents dans le mot de passe.
        if firstname != "" and firstname in password:
            PasswordGenerator.generate(firstname, lastname, length, use_digits, use_symbols)
        if lastname != "" and lastname in password:
            PasswordGenerator.generate(firstname, lastname, length, use_digits, use_symbols)
        return password


class PasswordGeneratorApp:
    def __init__(self):
        self.app = Flask(__name__)

        @self.app.route("/", methods=["GET", "POST"])
        def generate_password_page():
            empty_string = ""

            if request.method == "POST":
                lastname = request.form["lastname"]
                firstname = request.form["firstname"]
                length = int(request.form["length"])
                use_digits = "use_digits" in request.form
                use_symbols = "use_symbols" in request.form

                password = PasswordGenerator.generate(firstname, lastname, length, use_digits, use_symbols)

                # Definit la puissance du mot de passe
                power = password_power(length, use_digits, use_symbols)

                password_hash = hash_password(password)

                return render_template("index.html", password=password, power=power, password_hash=password_hash)

            return render_template("index.html", password=empty_string, power=empty_string, password_hash=empty_string)

    def run(self):
        self.app.run()


if __name__ == "__main__":
    # Crée une instance de PasswordGeneratorApp et exécute l'application.
    password_app = PasswordGeneratorApp()
    password_app.run()
